<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>4.1.0</string>
        <key>fileName</key>
        <string>F:/Game/Resources/Project/AR Defender Fantasy/Assets/TexturePacker/Map.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Resources/Sprites/TileMap.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap10.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap100.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap101.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap102.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap103.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap104.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap105.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap106.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap107.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap108.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap109.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap11.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap110.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap111.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap113.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap114.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap115.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap116.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap117.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap119.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap120.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap121.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap122.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap125.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap126.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap127.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap128.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap131.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap132.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap133.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap134.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap135.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap136.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap137.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap138.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap139.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap140.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap141.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap142.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap143.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap18.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap19.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap20.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap21.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap22.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap23.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap24.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap34.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap45.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap46.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap47.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap48.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap49.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap50.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap51.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap52.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap53.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap54.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap55.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap56.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap57.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap58.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap59.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap6.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap60.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap61.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap62.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap64.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap65.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap66.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap67.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap68.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap69.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap7.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap71.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap72.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap73.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap74.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap76.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap77.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap78.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap79.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap8.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap80.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap83.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap84.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap85.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap86.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap87.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap88.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap89.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap9.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap90.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap91.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap92.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap93.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap94.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap95.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap96.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap97.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap98.png</key>
            <key type="filename">C:/Users/DuyLuyen/Desktop/New folder/TileMap99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap47.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap48.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap49.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap50.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap51.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap52.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap53.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap54.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap55.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap56.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap57.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap58.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap59.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap60.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap61.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap62.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap64.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap65.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap66.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap67.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap68.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap69.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap71.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap72.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap73.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap74.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap76.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap77.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap78.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap79.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap80.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap83.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap84.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap85.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap86.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap87.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap88.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap89.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap90.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap91.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap92.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap93.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap94.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap95.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap96.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap97.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap98.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap99.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap100.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap101.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap102.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap103.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap104.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap105.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap106.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap107.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap108.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap109.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap110.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap111.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap113.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap114.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap115.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap116.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap117.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap119.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap120.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap121.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap122.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap125.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap126.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap127.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap128.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap131.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap132.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap133.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap134.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap135.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap136.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap137.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap138.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap139.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap140.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap141.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap142.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap143.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap6.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap7.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap8.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap9.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap10.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap11.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap18.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap19.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap20.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap21.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap22.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap23.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap24.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap34.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap45.png</filename>
            <filename>C:/Users/DuyLuyen/Desktop/New folder/TileMap46.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
