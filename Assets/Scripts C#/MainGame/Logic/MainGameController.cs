﻿using UnityEngine;
using System.Collections;
using System;

public partial class MainGameController : MonoBehaviour {

    public Transform ARCamera;

    public static MainGameController SharedInstance;

    public bool IsPlaying;
    public bool IsPausing;
    public CrystalTower Crystal;
    public TowerBehaviour[] Towers;
    void Awake()
    {
        if (SharedInstance == null)
        {
            SharedInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void OnTrackableFound()
    {
        if (!IsPlaying)
        {
            InitNewGame();
        }
        else
        {
            ResumeGame();
        }
    }

    public void OnTrackableLost()
    {
        if (IsPlaying)
        {
            PauseGame();
        }
    }
}
