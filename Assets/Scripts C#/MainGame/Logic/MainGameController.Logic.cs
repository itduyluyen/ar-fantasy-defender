﻿using UnityEngine;
using System.Collections;

public partial class MainGameController : MonoBehaviour {

	private void InitNewGame()
    {
        MapBehaviour.SharedInstance.StartGame();
        UIPanel.SharedInstance.StartGame();
        Crystal.BuildingTower();
        IsPlaying = true;
    }

    private void ResumeGame()
    {

    }

    private void PauseGame()
    {
        //set camera's angle to top-down
        ARCamera.position = GameVariables.DefaultCameraPosition;
        ARCamera.localEulerAngles = GameVariables.DefaultCameraRotation;
    }
}
