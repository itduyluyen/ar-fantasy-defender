﻿using UnityEngine;
using System.Collections;
using System;

public class TileBehaviour : MonoBehaviour, IGame {

    public bool IsActive;
    public Vector2 CellPosition;
    public SpriteRenderer TileSprite;

	public void InitTile()
    {
        TileSprite = GetComponent<SpriteRenderer>();
        //set image transparent
        Color c = TileSprite.color;
        c.a = 0;
        TileSprite.color = c;
    }

    void Update()
    {
        if (IsActive)
        {
            if (TileSprite.color.a < GameVariables.TilesetTransparentRate)
            {
                Color color = TileSprite.color;
                color.a += 0.5f * Time.deltaTime;
                TileSprite.color = color;
            }
            else
            {
                Color color = TileSprite.color;
                color.a = GameVariables.TilesetTransparentRate;
                TileSprite.color = color;
            }
        }
    }

    public void StartGame()
    {
        throw new NotImplementedException();
    }

    public void PauseGame()
    {
        throw new NotImplementedException();
    }

    public void ResumeGame()
    {
        throw new NotImplementedException();
    }

    public void EndGame()
    {
        throw new NotImplementedException();
    }
}
