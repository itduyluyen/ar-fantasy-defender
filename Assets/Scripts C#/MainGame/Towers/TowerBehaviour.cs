﻿using UnityEngine;
using System.Collections;
using System;

public class TowerBehaviour : MonoBehaviour {

	public virtual void BuildingTower()
    {
        gameObject.SetActive(true);
        StartCoroutine(Building());
    }

    private IEnumerator Building()
    {
        while (transform.localScale.x < 1)
        {
            yield return new WaitForSeconds(0.01f);
            transform.localScale += Vector3.one * 0.01f;
        }
        transform.localScale = Vector3.one;
    }
}
