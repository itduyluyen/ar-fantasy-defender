﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MapBehaviour : MonoBehaviour, IGame {

    public static MapBehaviour SharedInstance;
    public List<TileBehaviour> MapObjects = new List<TileBehaviour>();
    public Sprite[] Tiles;

    void Awake()
    {
        if (SharedInstance == null)
        {
            SharedInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        CreateMap();
    }

    private IEnumerator DisplayMapEffect()
    {
        int count = 0;
        while (count <= GameVariables.MapWidth / 2)
        {
            yield return new WaitForSeconds(0.2f);

            for (int index = 0; index < MapObjects.Count; index++)
            {
                if (Vector2.Distance(Vector2.zero, MapObjects[index].CellPosition) <= count * Mathf.Sqrt(2) + 0.1f && 
                    (Mathf.Abs(MapObjects[index].CellPosition.x) == count ||
                     Mathf.Abs(MapObjects[index].CellPosition.y) == count))
                {
                    MapObjects[index].IsActive = true;
                }
            }
            count++;
        }
    }

    private void CreateMap()
    {
        for (int heightCount = 0; heightCount < GameVariables.MapHeight; heightCount++)
        {
            for (int widthCount = 0; widthCount < GameVariables.MapWidth; widthCount++)
            {
                Vector2 pos = new Vector2(widthCount - GameVariables.MapWidth / 2, GameVariables.MapHeight / 2 - heightCount);
                //create new object
                GameObject obj = new GameObject();
                obj.name = "Tile[" + pos.x + "," + pos.y + "]";
                obj.transform.parent = transform;
                //add sprite renderer
                SpriteRenderer spr = obj.AddComponent<SpriteRenderer>();
                spr.sprite = Tiles[GameVariables.Map[widthCount][heightCount]];
                spr.enabled = true;
                //set position
                obj.transform.localPosition = new Vector3(spr.bounds.size.x * pos.x, 0, spr.bounds.size.y * pos.y);
                obj.transform.localEulerAngles = new Vector3(90, 0, 0);
                //add behaviour
                TileBehaviour tile = obj.AddComponent<TileBehaviour>();
                tile.CellPosition = pos;
                tile.InitTile();
                //add to list manager
                MapObjects.Add(tile);
            }
        }
    }

    public void OnTrackableFound()
    {
        if (MainGameController.SharedInstance.IsPlaying)
        {
            StartCoroutine(DisplayMapEffect());
        }
    }

    public void StartGame()
    {
        StartCoroutine(DisplayMapEffect());
    }

    void IGame.PauseGame()
    {
        throw new NotImplementedException();
    }

    public void ResumeGame()
    {
        throw new NotImplementedException();
    }

    public void EndGame()
    {
        throw new NotImplementedException();
    }
}
