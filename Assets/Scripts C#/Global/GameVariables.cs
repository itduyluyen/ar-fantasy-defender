﻿using UnityEngine;
using System.Collections;

public class GameVariables {

    public static int[][] Map = new int[][]
    {
        new int[] { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01, 00, 00 },
        new int[] { 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01, 00, 00 },
        new int[] { 01, 01, 01, 00, 00, 00, 00, 00, 00, 00, 01, 01, 01, 00, 00 },
        new int[] { 00, 00, 01, 00, 00, 00, 00, 00, 00, 01, 01, 00, 00, 00, 00 },
        new int[] { 00, 00, 01, 01, 00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00 },
        new int[] { 00, 00, 00, 01, 01, 01, 01, 01, 01, 01, 00, 00, 00, 00, 00 },
        new int[] { 00, 00, 00, 00, 00, 01, 02, 02, 02, 01, 00, 00, 00, 00, 00 },
        new int[] { 00, 00, 00, 00, 00, 01, 02, 02, 02, 01, 00, 00, 00, 00, 00 },
        new int[] { 00, 00, 00, 00, 00, 01, 02, 02, 02, 01, 00, 00, 00, 00, 00 },
        new int[] { 00, 00, 00, 00, 00, 01, 01, 01, 01, 01, 01, 01, 00, 00, 00 },
        new int[] { 00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 01, 01, 00, 00 },
        new int[] { 00, 00, 00, 00, 01, 01, 00, 00, 00, 00, 00, 00, 01, 00, 00 },
        new int[] { 00, 00, 01, 01, 01, 00, 00, 00, 00, 00, 00, 00, 01, 01, 01 },
        new int[] { 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 },
        new int[] { 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 }
    };

    public static int MapHeight = 15;
    public static int MapWidth = 15;

    public static float TilesetTransparentRate = 0.2f;
    public static Vector3 DefaultCameraPosition = new Vector3(0, 15, 0);
    public static Vector3 DefaultCameraRotation = new Vector3(90, 0, 0);
}
