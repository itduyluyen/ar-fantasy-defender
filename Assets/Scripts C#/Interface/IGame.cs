﻿using UnityEngine;
using System.Collections;

public interface IGame {

    void StartGame();

    void PauseGame();

    void ResumeGame();

    void EndGame();
}
