﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIPanel : MonoBehaviour, IGame
{
    public Text UICoin;
    public Text UIDiamond;
    public Image UIEnergy;
    public Image UIProgress;

    public static UIPanel SharedInstance;

    void Awake()
    {
        if (SharedInstance == null)
        {
            SharedInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        gameObject.SetActive(false);
    }

    public void Init()
    {
        SetCoin(0);
        SetDiamond(0);
        SetEnergy(0);
        SetProgress(0);
    }

    public void SetCoin(int coin)
    {
        UICoin.text = coin.ToString();
    }

    public void AddCoin(int coin)
    {
        UICoin.text = (GetCoin() + coin).ToString();
    }

    public int GetCoin()
    {
        return int.Parse(UICoin.text);
    }

    public void SetDiamond(int diamond)
    {
        UIDiamond.text = diamond.ToString();
    }

    public void AddDiamond(int diamond)
    {
        UIDiamond.text = (GetDiamond() + diamond).ToString();
    }

    public int GetDiamond()
    {
        return int.Parse(UIDiamond.text);
    }

    public void SetEnergy(float energy)
    {
        UIEnergy.fillAmount = energy / 100;
    }

    public void AddEnergy(float energy)
    {
        UIEnergy.fillAmount += energy / 100;
    }

    public float GetEnergy()
    {
        return UIEnergy.fillAmount;
    }

    public void SetProgress(float progress)
    {
        UIProgress.fillAmount = progress / 100;
    }

    public void AddProgress(float progress)
    {
        UIProgress.fillAmount += progress / 100;
    }

    public float GetProgress()
    {
        return UIProgress.fillAmount;
    }

    public void StartGame()
    {
        gameObject.SetActive(true);
        Init();
    }


    public void PauseGame()
    {
        
    }

    public void ResumeGame()
    {
        
    }

    public void EndGame()
    {
        throw new NotImplementedException();
    }
}
