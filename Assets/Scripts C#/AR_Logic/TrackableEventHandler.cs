﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System;

public class TrackableEventHandler : MonoBehaviour, ITrackableEventHandler {

    protected TrackableBehaviour mTrackableBehaviour;

    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || 
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED ||
            newStatus == TrackableBehaviour.Status.TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    public virtual void OnTrackingFound()
    {
        Debug.Log(mTrackableBehaviour.TrackableName + " Tracking Found");
    }

    public virtual void OnTrackingLost()
    {
        Debug.Log(mTrackableBehaviour.TrackableName + " Tracking Lost");
    }
}
