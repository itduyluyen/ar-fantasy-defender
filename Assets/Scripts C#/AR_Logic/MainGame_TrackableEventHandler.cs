﻿using UnityEngine;
using System.Collections;

public class MainGame_TrackableEventHandler : TrackableEventHandler {

    public Transform Map;

    public override void OnTrackingFound()
    {
        base.OnTrackingFound();
        MainGameController.SharedInstance.OnTrackableFound();
    }

    public override void OnTrackingLost()
    {
        base.OnTrackingLost();
        MainGameController.SharedInstance.OnTrackableLost();
    }
}
